import bodyParser from "body-parser"
import cors from "cors"
import express from "express"
// import fs from "fs"
import http from "http"
import serveStatic from "serve-static"

import { checkDatabases } from "./databases"
import * as dossiersController from "./routes/dossiers"
import * as senateursController from "./routes/senateurs"
import * as textesController from "./routes/textes"
import serverConfig from "./server_config"

const { NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const app = express()

// app.set("title", config.title)
app.set("trust proxy", serverConfig.proxy)

// Enable Express case-sensitive and strict options.
app.enable("case sensitive routing")
app.enable("strict routing")

if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}

app.use(cors())
// Sirv has no @types/sirv package yet => Use serve-static instead.
// app.use(sirv(serverConfig.dataDir, { dev }))
app.use(serveStatic(serverConfig.dataDir))
app.use(bodyParser.json({ limit: "5mb" }))

app.get("/dossiers", dossiersController.listLois)
app.get(
  "/dossiers/:loicod",
  dossiersController.requireLoi,
  dossiersController.accessLoi,
)

app.get("/senateurs", senateursController.listSenateurs)

app.get("/textes", textesController.listTextes)
app.get(
  "/textes/:texcod",
  textesController.requireTexte,
  textesController.accessTexte,
)
// app.get(
//   "/textes/:texcod/amendements",
//   textesController.requireTexte,
//   textesController.listTexteLoiAmendements,
// )
app.get(
  "/textes/:texcod/contenu",
  textesController.requireTexte,
  textesController.accessContenuTexte,
)

function startExpress() {
  const host = serverConfig.listen.host
  const port = serverConfig.listen.port || serverConfig.port
  const server = http.createServer(app)
  server.listen(port, host, () => {
    console.log(`Listening on ${host || "*"}:${port}...`)
    // // Set up the WebSocket for handling GraphQL subscriptions.
    // new SubscriptionServer(
    //   {
    //     execute,
    //     schema: graphqlController.schema,
    //     subscribe,
    //   },
    //   {
    //     server,
    //     path: "/subscriptions",
    //   },
    // )
  })
  // server.timeout = 30 * 60 * 1000 // 30 minutes (in milliseconds)
}

checkDatabases()
  .then(startExpress)
  .catch((error: any) => {
    console.log(error.stack || error)
    process.exit(1)
  })
