import PgPromise from "pg-promise"

import serverConfig from "./server_config"

export const pgPromise = PgPromise()
export const dbNames = ["ameli", "debats", "dosleg", "questions", "sens"]
export const dbByName: { [name: string]: any } = {}
for (const name of dbNames) {
  dbByName[name] = pgPromise({
    database: name,
    host: serverConfig.db.host,
    password: serverConfig.db.password,
    port: serverConfig.db.port,
    user: serverConfig.db.user,
  })
}
export let dbSharedConnectionObjectByName: {
  [name: string]: PgPromise.IConnected<unknown>
} = {}

export async function checkDatabase(name: string) {
  // Check that database exists.
  const db = dbByName[name]
  dbSharedConnectionObjectByName[name] = await db.connect()
  return db
}

export async function checkDatabases() {
  for (const name of dbNames) {
    await checkDatabase(name)
  }
}

export function parseIntFields(
  fieldsNames: string[],
  entry: { [fieldName: string]: any } | null,
): { [fieldName: string]: any } | null {
  if (entry === null) {
    return null
  }
  for (const fieldName of fieldsNames) {
    if (entry[fieldName] !== null) {
      entry[fieldName] = parseInt(entry[fieldName])
    }
  }
  return entry
}

export function trimFieldsRight(
  fieldsNames: string[],
  entry: { [fieldName: string]: any } | null,
): { [fieldName: string]: any } | null {
  if (entry === null) {
    return null
  }
  for (const fieldName of fieldsNames) {
    if (entry[fieldName] !== null) {
      entry[fieldName] = entry[fieldName].trimEnd()
    }
  }
  return entry
}
