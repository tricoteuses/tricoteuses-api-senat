import assert from "assert"
import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
// import fetch from "node-fetch"
import path from "path"
// import stream from "stream"
import StreamZip from "node-stream-zip"
// import util from "util"
import windows1252 from "windows-1252"

import { datasets, Dataset, EnabledDatasets } from "../datasets"

const badWindows1252CharacterRegex = /[\u0080-\u009f]/g
const optionsDefinitions = [
  {
    alias: "a",
    help: "all options: fetch, unzip, repair-encoding, import, patch, schema",
    name: "all",
    type: Boolean,
  },
  {
    alias: "c",
    help: "create TypeScript interfaces from databases schemas into given @tricoteuses/senat directory",
    name: "schema",
    type: String,
  },
  {
    alias: "e",
    help: "repair Windows CP 1252 encoding of SQL dumps",
    name: "repair-encoding",
    type: Boolean,
  },
  {
    alias: "f",
    help: "fetch datasets instead of retrieving them from files",
    name: "fetch",
    type: Boolean,
  },
  {
    alias: "i",
    help: "import SQL dumps into a freshly (re-)created database",
    name: "import",
    type: Boolean,
  },
  {
    alias: "p",
    help: "patch database to add Tricoteuses-specific features",
    name: "patch",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "o",
    help: 'use sudo for "psql" command',
    name: "sudo",
    type: Boolean,
  },
  {
    alias: "u",
    help: "optional name of user for sudo command",
    name: "user",
    type: String,
  },
  {
    alias: "z",
    help: "unzip SQL files",
    name: "unzip",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Sénat open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

// const pipeline = util.promisify(stream.pipeline)

async function retrieveDataset(dataDir: string, dataset: Dataset): Promise<void> {
  const zipFilename = dataset.url.substring(dataset.url.lastIndexOf("/") + 1)
  const zipFilePath = path.join(dataDir, zipFilename)
  if (options.all || options.fetch) {
    // Fetch & save ZIP file.
    if (!options.silent) {
      console.log(`Loading ${dataset.title}: ${zipFilename}…`)
    }
    // Fetch fails with OpenSSL error: dh key too small.
    // (so does "curl").
    // const response = await fetch(dataset.url)
    // if (!response.ok) {
    //   console.error(response.status, response.statusText)
    //   console.error(await response.text())
    //   throw new Error(`Fetch failed: ${dataset.url}`)
    // }
    // await pipeline(response.body!, fs.createWriteStream(zipFilePath))
    fs.removeSync(zipFilePath)
    execSync(`wget --quiet ${dataset.url}`, {
      cwd: dataDir,
      env: process.env,
      encoding: "utf-8",
      // stdio: ["ignore", "ignore", "pipe"],
    })
  }

  const sqlFilename = `${dataset.database}.sql`
  const sqlFilePath = path.join(dataDir, sqlFilename)
  if (options.all || options.unzip) {
    if (!options.silent) {
      console.log(`Unzipping ${dataset.title}: ${zipFilename}…`)
    }
    fs.removeSync(sqlFilePath)
    const zip = new StreamZip({
      file: zipFilePath,
      storeEntries: true,
    })
    await new Promise((resolve, reject) => {
      zip.on("ready", () => {
        zip.extract(null, dataDir, (err: any, _count: number) => {
          zip.close()
          if (err) {
            reject(err)
          } else {
            resolve()
          }
        })
      })
    })
    if (dataset.repairZip !== undefined) {
      if (!options.silent) {
        console.log(`Repairing Zip path ${dataset.title}: ${sqlFilename}…`)
      }
      dataset.repairZip(dataset, dataDir)
    }
  }

  if ((options.all || options["repair-encoding"]) && dataset.repairEncoding) {
    if (!options.silent) {
      console.log(
        `Repairing Windows CP1252 encoding of ${dataset.title}: ${sqlFilename}…`,
      )
    }
    const sql = fs.readFileSync(sqlFilePath, { encoding: "utf8" })
    const sqlRepaired = sql.replace(badWindows1252CharacterRegex, match =>
      windows1252.decode(match, { mode: "fatal" }),
    )
    fs.writeFileSync(sqlFilePath, sqlRepaired)
  }

  if (options.all || options.import) {
    if (!options.silent) {
      console.log(`Importing ${dataset.title}: ${sqlFilename}…`)
    }
    if (options.sudo) {
      if (options.user) {
        execSync(
          `sudo -u ${options.user} psql -c "DROP DATABASE IF EXISTS ${dataset.database}"`,
          {
            cwd: dataDir,
            env: process.env,
            encoding: "utf-8",
            // stdio: ["ignore", "ignore", "pipe"],
          },
        )
        execSync(
          `sudo -u ${options.user} psql -c "CREATE DATABASE ${dataset.database} WITH OWNER opendata"`,
          {
            cwd: dataDir,
            env: process.env,
            encoding: "utf-8",
            // stdio: ["ignore", "ignore", "pipe"],
          },
        )
        execSync(`sudo -u ${options.user} psql -f ${sqlFilename} ${dataset.database}`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          // stdio: ["ignore", "ignore", "pipe"],
        })
      } else {
        execSync(`sudo psql -c "DROP DATABASE IF EXISTS ${dataset.database}"`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          // stdio: ["ignore", "ignore", "pipe"],
        })
        execSync(
          `sudo psql -c "CREATE DATABASE ${dataset.database} WITH OWNER opendata"`,
          {
            cwd: dataDir,
            env: process.env,
            encoding: "utf-8",
            // stdio: ["ignore", "ignore", "pipe"],
          },
        )
        execSync(`sudo psql -f ${sqlFilename} ${dataset.database}`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          // stdio: ["ignore", "ignore", "pipe"],
        })
      }
    } else {
      execSync(`psql -c "DROP DATABASE IF EXISTS ${dataset.database}"`, {
        cwd: dataDir,
        env: process.env,
        encoding: "utf-8",
        // stdio: ["ignore", "ignore", "pipe"],
      })
      execSync(`psql -c "CREATE DATABASE ${dataset.database} WITH OWNER opendata"`, {
        cwd: dataDir,
        env: process.env,
        encoding: "utf-8",
        // stdio: ["ignore", "ignore", "pipe"],
      })
      execSync(`psql -f ${sqlFilename} ${dataset.database}`, {
        cwd: dataDir,
        env: process.env,
        encoding: "utf-8",
        // stdio: ["ignore", "ignore", "pipe"],
      })
    }
  }

  if ((options.all || options.patch) && dataset.patch !== undefined) {
    if (!options.silent) {
      console.log(`Adding Tricoteuses patches to database ${dataset.database}…`)
    }
    await dataset.patch(dataset, dataDir, { sudo: options.sudo, user: options.user })
  }

  if (options.schema) {
    const definitionsDir = path.join(options.schema, "src/raw_types/")
    assert(fs.statSync(definitionsDir).isDirectory())
    if (!options.silent) {
      console.log(
        `Creating TypeScript definitions from schema of database ${dataset.database}…`,
      )
    }
    const definitionFilePath = path.join(definitionsDir, `${dataset.database}.ts`)
    execSync(
      `npx schemats generate -c postgresql://opendata:opendata@localhost:5432/${dataset.database} -s public -o ${definitionFilePath}`,
      {
        // cwd: dataDir,
        env: process.env,
        encoding: "utf-8",
        // stdio: ["ignore", "ignore", "pipe"],
      },
    )
    const definition = fs.readFileSync(definitionFilePath, { encoding: "utf8" })
    const definitionRepaired = definition
      .replace(/\r\n/g, "\n")
      .replace(
        /AUTO-GENERATED FILE @ \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/,
        "AUTO-GENERATED FILE",
      )
    fs.writeFileSync(definitionFilePath, definitionRepaired)
  }
}

async function retrieveOpenData(
  dataDir: string,
  enabledDatasets: EnabledDatasets,
): Promise<void> {
  const choosenDatasets: Dataset[] = [
    enabledDatasets & EnabledDatasets.Ameli ? datasets.ameli : null,
    enabledDatasets & EnabledDatasets.Debats ? datasets.debats : null,
    enabledDatasets & EnabledDatasets.DosLeg ? datasets.dosleg : null,
    enabledDatasets & EnabledDatasets.Questions ? datasets.questions : null,
    enabledDatasets & EnabledDatasets.Sens ? datasets.sens : null,
  ].filter((dataset: Dataset | null) => dataset !== null) as Dataset[]
  // await Promise.all(choosenDatasets.map(dataset => retrieveDataset(dataDir, dataset)))
  for (const dataset of choosenDatasets) {
    await retrieveDataset(dataDir, dataset)
  }
  process.exit(0)
}

retrieveOpenData(options.dataDir, EnabledDatasets.All).catch(error => {
  console.log(error)
  process.exit(1)
})
