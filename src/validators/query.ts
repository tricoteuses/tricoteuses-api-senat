import {
  validateArray,
  validateChain,
  validateChoice,
  validateFunction,
  validateMissing,
  validateOption,
  validateSetValue,
  validateString,
  Validator,
} from "@biryani/core"

export function validateFollow(possibleValues: string[]): Validator {
  return validateChain(
    validateOption(
      [validateMissing, validateSetValue([])],
      [validateString, validateFunction(value => [value])],
      validateArray(validateString),
    ),
    validateArray(validateChoice(possibleValues)),
    validateFunction(values => new Set(values)),
  )
}
