import {
  Ass,
  Aud,
  Auteur,
  DateSeance,
  Debat,
  DecCoc,
  DenRap,
  DocAtt,
  Ecr,
  EtaLoi,
  LecAss,
  LecAssDeb,
  LecAssRap,
  Lecture,
  Loi,
  Org,
  OriTxt,
  Qua,
  Rap,
  RapOrg,
  Scr,
  Ses,
  Sub,
  Texte,
  TxtAmeli,
  TypAtt,
  TypLec,
  TypLoi,
  TypTxt,
  TypUrl,
} from "@tricoteuses/senat"

import { dbByName, trimFieldsRight } from "../databases"
import {
  getSessFromAnns,
  getSubsFromTxtids,
  getTxtsAmeliBySesannNumJoins,
} from "./ameli"
import {
  getDebats,
  getDebatsFromLecassidts,
  lecassdebFieldsToTrim,
} from "./debats"
import {
  getAsss,
  getAudsFromLecassidts,
  getAuteurs,
  getDatesSeancesFromLecassidts,
  getDeccocs,
  getDenraps,
  getDocattsFromRapcods,
  getEcrsFromRapcods,
  getEcrsFromTexcods,
  getEtalois,
  getLecassrapsFromLecassidts,
  getLecasssFromLecidts,
  getLecturesFromLoicods,
  getQuas,
  getTextesFromLecassidts,
  getLois,
  getOrgs,
  getOrgsFromRapcods,
  getOritxts,
  getRaps,
  getScrsFromCodes,
  getTypatts,
  getTyplecs,
  getTyplois,
  getTyptxts,
  getTypurls,
  raporgFieldsToTrim,
} from "./dosleg"

export const allFollows = [
  "aud.org",
  "auteur.qua",
  "dateSeance.debat",
  "dateSeance.scrs",
  "docatt.rec",
  "docatt.typatt",
  "ecr.aut",
  "lecass.ass",
  "lecass.auds",
  "lecass.datesSeances",
  "lecass.debats",
  "lecass.lecassraps",
  "lecass.org",
  "lecass.textes",
  "lecassrap.rap",
  "lecture.lecasss",
  "lecture.typlec",
  "loi.deccoc",
  "loi.etaloi",
  "loi.lectures",
  "loi.typloi",
  "rap.denrap",
  "rap.docatts",
  "rap.ecrs",
  "rap.orgs",
  "texte.ecrs",
  "texte.org",
  "texte.oritxt",
  "texte.txtAmeli",
  "texte.typtxt",
  "texte.typurl",
  "txtAmeli.subs",
]

export class DataGetter {
  objectByIdByTableName: { [name: string]: { [id: string]: object } } = {}
  requestedIdsByTableName: { [name: string]: Set<string> } = {}
  visitedIdsByTableName: { [name: string]: Set<string> } = {}

  constructor(public follow: Set<string>) {}

  addAss(ass: Ass): void {
    let objectById = this.objectByIdByTableName.ass
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.ass = {}
    }
    objectById[ass.codass] = ass
  }

  addAud(aud: Aud): void {
    let objectById = this.objectByIdByTableName.aud
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.aud = {}
    }
    objectById[aud.audcle] = aud

    if (aud.orgcod !== null && this.follow.has("aud.org")) {
      this.request("org", aud.orgcod)
    }
  }

  addAuteur(auteur: Auteur): void {
    let objectById = this.objectByIdByTableName.auteur
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.auteur = {}
    }
    objectById[auteur.autcod] = auteur

    if (this.follow.has("auteur.qua")) {
      this.request("qua", auteur.quacod)
    }

    // TODO
  }

  addDateSeance(dateSeance: DateSeance): void {
    let objectById = this.objectByIdByTableName.date_seance
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.date_seance = {}
    }
    objectById[dateSeance.code] = dateSeance

    if (dateSeance.date_s !== null && this.follow.has("dateSeance.debat")) {
      this.request("debats", dateSeance.date_s.toISOString())
    }

    if (this.follow.has("dateSeance.scrs")) {
      this.request("scr_by_code", dateSeance.code.toString())
    }
  }

  addDebat(debat: Debat): void {
    let objectById = this.objectByIdByTableName.debats
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.debats = {}
    }
    objectById[debat.datsea.toISOString()] = debat
  }

  addDeccoc(deccoc: DecCoc): void {
    let objectById = this.objectByIdByTableName.deccoc
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.deccoc = {}
    }
    objectById[deccoc.deccoccod] = deccoc
  }

  addDenrap(denrap: DenRap): void {
    let objectById = this.objectByIdByTableName.denrap
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.denrap = {}
    }
    objectById[denrap.coddenrap] = denrap
  }

  addDocatt(docatt: DocAtt): void {
    let objectById = this.objectByIdByTableName.docatt
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.docatt = {}
    }
    objectById[docatt.docattcle] = docatt


    if (this.follow.has("docatt.rap")) {
      this.request("rap", docatt.rapcod.toString())
    }

    if (this.follow.has("docatt.typatt")) {
      this.request("typatt", docatt.rapcod.toString())
    }
  }

  addEcr(ecr: Ecr): void {
    let objectById = this.objectByIdByTableName.ecr
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.ecr = {}
    }
    objectById[ecr.ecrnum] = ecr

    if (ecr.autcod !== null && this.follow.has("ecr.aut")) {
      this.request("auteur", ecr.autcod)
    }
  }

  addEtaloi(etaloi: EtaLoi): void {
    let objectById = this.objectByIdByTableName.etaloi
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.etaloi = {}
    }
    objectById[etaloi.etaloicod] = etaloi
  }

  addLecass(lecass: LecAss): void {
    let objectById = this.objectByIdByTableName.lecass
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.lecass = {}
    }
    objectById[lecass.lecassidt] = lecass

    if (this.follow.has("lecass.ass")) {
      this.request("ass", lecass.codass)
    }

    if (lecass.orgcod !== null && this.follow.has("lecass.org")) {
      this.request("org", lecass.orgcod)
    }

    if (this.follow.has("lecass.auds")) {
      this.request("aud_by_lecassidt", lecass.lecassidt)
    }

    if (this.follow.has("lecass.datesSeances")) {
      this.request("date_seance_by_lecassidt", lecass.lecassidt)
    }

    if (this.follow.has("lecass.debats")) {
      this.request("debat_by_lecassidt", lecass.lecassidt)
    }

    if (this.follow.has("lecass.lecassraps")) {
      this.request("lecassrap_by_lecassidt", lecass.lecassidt)
    }

    if (this.follow.has("lecass.textes")) {
      this.request("texte_by_lecassidt", lecass.lecassidt)
    }
  }

  addLecassrap(lecassrap: LecAssRap): void {
    let objectById = this.objectByIdByTableName.lecassrap
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.lecassrap = {}
    }
    objectById[`${lecassrap.lecassidt} ${lecassrap.rapcod}`] = lecassrap

    if (this.follow.has("lecassrap.rap")) {
      this.request("rap", lecassrap.rapcod.toString())
    }
  }

  addLecture(lecture: Lecture): void {
    let objectById = this.objectByIdByTableName.lecture
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.lecture = {}
    }
    objectById[lecture.lecidt] = lecture

    if (this.follow.has("lecture.typlec")) {
      this.request("typlec", lecture.typleccod)
    }

    if (this.follow.has("lecture.lecasss")) {
      this.request("lecasss_by_lecidt", lecture.lecidt)
    }
  }

  addLoi(loi: Loi): void {
    let objectById = this.objectByIdByTableName.loi
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.loi = {}
    }
    objectById[loi.loicod] = loi

    if (this.follow.has("loi.typloi")) {
      this.request("typloi", loi.typloicod)
    }

    if (this.follow.has("loi.etaloi") && loi.etaloicod !== null) {
      this.request("etaloi", loi.etaloicod)
    }

    if (this.follow.has("loi.deccoc") && loi.deccoccod !== null) {
      this.request("deccoc", loi.deccoccod)
    }

    if (this.follow.has("loi.lectures")) {
      this.request("lectures_by_loicod", loi.loicod)
    }
  }

  addOrg(org: Org): void {
    let objectById = this.objectByIdByTableName.org
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.org = {}
    }
    objectById[org.orgcod] = org

    // TODO
  }

  addOritxt(oritxt: OriTxt): void {
    let objectById = this.objectByIdByTableName.oritxt
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.oritxt = {}
    }
    objectById[oritxt.oritxtcod] = oritxt

    // TODO
  }

  addQua(qua: Qua): void {
    let objectById = this.objectByIdByTableName.qua
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.qua = {}
    }
    objectById[qua.quacod] = qua
  }

  addRap(rap: Rap): void {
    let objectById = this.objectByIdByTableName.rap
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.rap = {}
    }
    objectById[rap.rapcod.toString()] = rap

    if (this.follow.has("rap.denrap")) {
      this.request("denrap", rap.coddenrap)
    }

    if (this.follow.has("rap.docatts")) {
      this.request("docatt_by_rapcod", rap.rapcod.toString())
    }

    if (this.follow.has("rap.ecrs")) {
      this.request("ecr_by_rapcod", rap.rapcod.toString())
    }

    if (this.follow.has("rap.orgs")) {
      this.request("org_by_rapcod", rap.rapcod.toString())
    }

    // TODO
  }

  addScr(scr: Scr): void {
    let objectById = this.objectByIdByTableName.scr
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.scr = {}
    }
    objectById[`${scr.sesann} ${scr.scrnum}`] = scr

    // TODO
  }

  addSes(ses: Ses): void {
    let objectById = this.objectByIdByTableName.ses
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.ses = {}
    }
    objectById[ses.id] = ses
  }

  addSub(sub: Sub): void {
    let objectById = this.objectByIdByTableName.sub
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.sub = {}
    }
    objectById[sub.id] = sub

    // TODO
  }

  addTexte(texte: Texte): void {
    let objectById = this.objectByIdByTableName.texte
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.texte = {}
    }
    objectById[texte.texcod] = texte

    if (this.follow.has("texte.ecrs")) {
      this.request("ecr_by_texcod", texte.texcod.toString())
    }

    if (texte.orgcod !== null && this.follow.has("texte.org")) {
      this.request("org", texte.orgcod)
    }

    if (texte.oritxtcod !== null && this.follow.has("texte.oritxt")) {
      this.request("oritxt", texte.oritxtcod)
    }

    if (texte.sesann !== null && texte.texnum !== null && this.follow.has("texte.txtAmeli")) {
      this.request("ses_by_ann", texte.sesann.toString())
      this.request("txt_ameli_by_sesann_num", `${texte.sesann} ${texte.texnum}`)
    }

    if (texte.typtxtcod !== null && this.follow.has("texte.typtxt")) {
      this.request("typtxt", texte.typtxtcod)
    }

    if (texte.typurl !== null && this.follow.has("texte.typurl")) {
      this.request("typurl", texte.typurl)
    }
  }

  addTxtAmeli(txtAmeli: TxtAmeli): void {
    let objectById = this.objectByIdByTableName.txt_ameli
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.txt_ameli = {}
    }
    objectById[txtAmeli.id] = txtAmeli

    if (this.follow.has("txtAmeli.subs")) {
      this.request("sub_by_txtid", txtAmeli.id.toString())
    }

    // TODO
   }

  addTypatt(typatt: TypAtt): void {
    let objectById = this.objectByIdByTableName.typatt
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.typatt = {}
    }
    objectById[typatt.typattcod] = typatt
  }

  addTyplec(typlec: TypLec): void {
    let objectById = this.objectByIdByTableName.typlec
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.typlec = {}
    }
    objectById[typlec.typleccod] = typlec
  }

  addTyploi(typloi: TypLoi): void {
    let objectById = this.objectByIdByTableName.typloi
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.typloi = {}
    }
    objectById[typloi.typloicod] = typloi

    // TODO
  }

  addTyptxt(typtxt: TypTxt): void {
    let objectById = this.objectByIdByTableName.typtxt
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.typtxt = {}
    }
    objectById[typtxt.typtxtcod] = typtxt
  }

  addTypurl(typurl: TypUrl): void {
    let objectById = this.objectByIdByTableName.typurl
    if (objectById === undefined) {
      objectById = this.objectByIdByTableName.typurl = {}
    }
    objectById[typurl.typurl] = typurl
  }

  async getAll(): Promise<void> {
    while (Object.keys(this.requestedIdsByTableName).length > 0) {
      for (const [tableName, requestedIdsSet] of Object.entries(
        this.requestedIdsByTableName,
      )) {
        const requestedIds = [...requestedIdsSet]
        delete this.requestedIdsByTableName[tableName]
        switch (tableName) {
          case "ass": {
            ;(await getAsss(requestedIds)).map(this.addAss.bind(this))
            break
          }
          case "aud_by_lecassidt": {
            ;(await getAudsFromLecassidts(requestedIds)).map(this.addAud.bind(this))
            break
          }
          case "auteur": {
            ;(await getAuteurs(requestedIds)).map(this.addAuteur.bind(this))
            break
          }
          case "date_seance_by_lecassidt": {
            ;(await getDatesSeancesFromLecassidts(requestedIds)).map(
              this.addDateSeance.bind(this),
            )
            break
          }
          case "debat_by_lecassidt": {
            ;(await getDebatsFromLecassidts(requestedIds)).map(this.addDebat.bind(this))
            break
          }
          case "debats": {
            ;(await getDebats(requestedIds)).map(this.addDebat.bind(this))
            break
          }
          case "deccoc": {
            ;(await getDeccocs(requestedIds)).map(this.addDeccoc.bind(this))
            break
          }
          case "denrap": {
            ;(await getDenraps(requestedIds)).map(this.addDenrap.bind(this))
            break
          }
          case "docatt_by_rapcod": {
            ;(await getDocattsFromRapcods(requestedIds)).map(this.addDocatt.bind(this))
            break
          }
          case "ecr_by_rapcod": {
            ;(await getEcrsFromRapcods(requestedIds)).map(this.addEcr.bind(this))
            break
          }
          case "ecr_by_texcod": {
            ;(await getEcrsFromTexcods(requestedIds)).map(this.addEcr.bind(this))
            break
          }
          case "etaloi": {
            ;(await getEtalois(requestedIds)).map(this.addEtaloi.bind(this))
            break
          }
          case "lecasss_by_lecidt": {
            ;(await getLecasssFromLecidts(requestedIds)).map(this.addLecass.bind(this))
            break
          }
          case "lecassrap_by_lecassidt": {
            ;(await getLecassrapsFromLecassidts(requestedIds)).map(
              this.addLecassrap.bind(this),
            )
            break
          }
          case "lectures_by_loicod": {
            ;(await getLecturesFromLoicods(requestedIds)).map(this.addLecture.bind(this))
            break
          }
          case "loi": {
            ;(await getLois(requestedIds)).map(this.addLoi.bind(this))
            break
          }
          case "org": {
            ;(await getOrgs(requestedIds)).map(this.addOrg.bind(this))
            break
          }
          case "org_by_rapcod": {
            ;(await getOrgsFromRapcods(requestedIds)).map(this.addOrg.bind(this))
            break
          }
          case "oritxt": {
            ;(await getOritxts(requestedIds)).map(this.addOritxt.bind(this))
            break
          }
          case "qua": {
            ;(await getQuas(requestedIds)).map(this.addQua.bind(this))
            break
          }
          case "rap": {
            ;(await getRaps(requestedIds)).map(this.addRap.bind(this))
            break
          }
          case "scr_by_code": {
            ;(await getScrsFromCodes(requestedIds)).map(this.addScr.bind(this))
            break
          }
          case "ses_by_ann": {
            ;(await getSessFromAnns(requestedIds)).map(this.addSes.bind(this))
            break
          }
          case "sub_by_txtid": {
            ;(await getSubsFromTxtids(requestedIds)).map(this.addSub.bind(this))
            break
          }
          case "texte_by_lecassidt": {
            ;(await getTextesFromLecassidts(requestedIds)).map(this.addTexte.bind(this))
            break
          }
          case "txt_ameli_by_sesann_num": {
            ;(await getTxtsAmeliBySesannNumJoins(requestedIds)).map(this.addTxtAmeli.bind(this))
            break
          }
          case "typatt": {
            ;(await getTypatts(requestedIds)).map(this.addTypatt.bind(this))
            break
          }
          case "typlec": {
            ;(await getTyplecs(requestedIds)).map(this.addTyplec.bind(this))
            break
          }
          case "typloi": {
            ;(await getTyplois(requestedIds)).map(this.addTyploi.bind(this))
            break
          }
          case "typtxt": {
            ;(await getTyptxts(requestedIds)).map(this.addTyptxt.bind(this))
            break
          }
          case "typurl": {
            ;(await getTypurls(requestedIds)).map(this.addTypurl.bind(this))
            break
          }
          default:
            throw new Error(`Unhandled table: ${tableName}`)
        }
        let visitedIds = this.visitedIdsByTableName[tableName]
        this.visitedIdsByTableName[tableName] =
          visitedIds === undefined
            ? new Set(requestedIds)
            : new Set([...visitedIds, ...requestedIds])
      }
    }

    // Add "1 to many" and other computed references.
    if (this.follow.has("dateSeance.scrs")) {
      const scrById = this.objectByIdByTableName.scr
      const dateSeanceByCode = this.objectByIdByTableName.date_seance
      if (scrById !== undefined && dateSeanceByCode !== undefined) {
        for (const scr of Object.values(scrById) as Scr[]) {
          const scrCode = scr.code
          if (scrCode === null) {
            continue
          }
          const dateSeance = dateSeanceByCode[scrCode] as DateSeance
          if (dateSeance !== undefined) {
            let scrids = dateSeance.scrids
            if (scrids === undefined) {
              scrids = dateSeance.scrids = []
            }
            scrids.push(`${scr.sesann} ${scr.scrnum}`)
          }
        }
      }
    }
    if (this.follow.has("lecass.auds")) {
      const audByAudcle = this.objectByIdByTableName.aud
      const lecassByLecassidt = this.objectByIdByTableName.lecass
      if (audByAudcle !== undefined && lecassByLecassidt !== undefined) {
        for (const aud of Object.values(audByAudcle) as Aud[]) {
          const lecass = lecassByLecassidt[aud.lecassidt] as LecAss
          if (lecass !== undefined) {
            let audcles = lecass.audcles
            if (audcles === undefined) {
              audcles = lecass.audcles = []
            }
            audcles.push(aud.audcle)
          }
        }
      }
    }
    if (this.follow.has("lecass.datesSeances")) {
      const dateSeanceByCode = this.objectByIdByTableName.date_seance
      const lecassByLecassidt = this.objectByIdByTableName.lecass
      if (dateSeanceByCode !== undefined && lecassByLecassidt !== undefined) {
        for (const dateSeance of Object.values(dateSeanceByCode) as DateSeance[]) {
          const lecassidt = dateSeance.lecidt
          if (lecassidt === null) {
            continue
          }
          const lecass = lecassByLecassidt[lecassidt] as LecAss
          if (lecass !== undefined) {
            let datesSeancesCodes = lecass.datesSeancesCodes
            if (datesSeancesCodes === undefined) {
              datesSeancesCodes = lecass.datesSeancesCodes = []
            }
            datesSeancesCodes.push(dateSeance.code)
          }
        }
      }
    }
    if (this.follow.has("lecass.debats")) {
      const debatById = this.objectByIdByTableName.debats
      const lecassByLecassidt = this.objectByIdByTableName.lecass
      if (debatById !== undefined && lecassByLecassidt !== undefined) {
        const lecassdebs = (await dbByName.debats.any(
          `
            SELECT *
            FROM lecassdeb
            WHERE datsea IN ($<datseas:list>)
          `,
          {
            datseas: Object.keys(debatById),
          },
        )).map((lecassdeb: LecAssDeb) =>
          trimFieldsRight(lecassdebFieldsToTrim, lecassdeb),
        )
        for (const lecassdeb of lecassdebs) {
          const lecass = lecassByLecassidt[lecassdeb.lecassidt] as LecAss
          if (lecass !== undefined) {
            let debatdatseas = lecass.debatdatseas
            if (debatdatseas === undefined) {
              debatdatseas = lecass.debatdatseas = []
            }
            debatdatseas.push(lecassdeb.datsea)
          }
        }
      }
    }
    if (this.follow.has("lecass.lecassraps")) {
      const lecassrapById = this.objectByIdByTableName.lecassrap
      const lecassByLecassidt = this.objectByIdByTableName.lecass
      if (lecassrapById !== undefined && lecassByLecassidt !== undefined) {
        for (const lecassrap of Object.values(lecassrapById) as LecAssRap[]) {
          const lecass = lecassByLecassidt[lecassrap.lecassidt] as LecAss
          if (lecass !== undefined) {
            let lecassrapids = lecass.lecassrapids
            if (lecassrapids === undefined) {
              lecassrapids = lecass.lecassrapids = []
            }
            lecassrapids.push(
              `${lecassrap.lecassidt} ${lecassrap.rapcod}`,
            )
          }
        }
      }
    }
    if (this.follow.has("lecass.textes")) {
      const texteByTexcod = this.objectByIdByTableName.texte
      const lecassByLecassidt = this.objectByIdByTableName.lecass
      if (texteByTexcod !== undefined && lecassByLecassidt !== undefined) {
        for (const texte of Object.values(texteByTexcod) as Texte[]) {
          const lecass = lecassByLecassidt[texte.lecassidt] as LecAss
          if (lecass !== undefined) {
            let texcods = lecass.texcods
            if (texcods === undefined) {
              texcods = lecass.texcods = []
            }
            texcods.push(texte.texcod)
          }
        }
      }
    }
    if (this.follow.has("lecture.lecasss")) {
      const lecassByLecassidt = this.objectByIdByTableName.lecass
      const lectureByLecidt = this.objectByIdByTableName.lecture
      if (lecassByLecassidt !== undefined && lectureByLecidt !== undefined) {
        for (const lecass of Object.values(lecassByLecassidt) as LecAss[]) {
          const lecture = lectureByLecidt[lecass.lecidt] as Lecture
          if (lecture !== undefined) {
            let lecassidts = lecture.lecassidts
            if (lecassidts === undefined) {
              lecassidts = lecture.lecassidts = []
            }
            lecassidts.push(lecass.lecassidt)
          }
        }
      }
    }
    if (this.follow.has("loi.lectures")) {
      const lectureByLecidt = this.objectByIdByTableName.lecture
      const loiByLoicod = this.objectByIdByTableName.loi
      if (lectureByLecidt !== undefined && loiByLoicod !== undefined) {
        for (const lecture of Object.values(lectureByLecidt) as Lecture[]) {
          const loi = loiByLoicod[lecture.loicod] as Loi
          if (loi !== undefined) {
            let lecidts = loi.lecidts
            if (lecidts === undefined) {
              lecidts = loi.lecidts = []
            }
            lecidts.push(lecture.lecidt)
          }
        }
      }
    }
    if (this.follow.has("rap.docatts")) {
      const docattByDocattcle = this.objectByIdByTableName.docatt
      const rapById = this.objectByIdByTableName.rap
      if (docattByDocattcle !== undefined && rapById !== undefined) {
        for (const docatt of Object.values(docattByDocattcle) as DocAtt[]) {
          const rapcod = docatt.rapcod
          if (rapcod === null) {
            continue
          }
          const rap = rapById[rapcod] as Rap
          if (rap !== undefined) {
            let docattcles = rap.docattcles
            if (docattcles === undefined) {
              docattcles = rap.docattcles = []
            }
            docattcles.push(docatt.docattcle)
          }
        }
      }
    }
    if (this.follow.has("rap.ecrs")) {
      const ecrByEcrnum = this.objectByIdByTableName.ecr
      const rapById = this.objectByIdByTableName.rap
      if (ecrByEcrnum !== undefined && rapById !== undefined) {
        for (const ecr of Object.values(ecrByEcrnum) as Ecr[]) {
          const rapcod = ecr.rapcod
          if (rapcod === null) {
            continue
          }
          const rap = rapById[rapcod] as Rap
          if (rap !== undefined) {
            let ecrnums = rap.ecrnums
            if (ecrnums === undefined) {
              ecrnums = rap.ecrnums = []
            }
            ecrnums.push(ecr.ecrnum)
          }
        }
      }
    }
    if (this.follow.has("rap.orgs")) {
      const orgByOrgcod = this.objectByIdByTableName.org
      const rapById = this.objectByIdByTableName.rap
      if (orgByOrgcod !== undefined && rapById !== undefined) {
        const raporgs = (await dbByName.dosleg.any(
          `
            SELECT *
            FROM raporg
            WHERE orgcod IN ($<orgcods:list>)
          `,
          {
            orgcods: Object.keys(orgByOrgcod),
          },
        )).map((raporg: RapOrg) => trimFieldsRight(raporgFieldsToTrim, raporg))
        for (const raporg of raporgs) {
          const rap = rapById[raporg.rapcod.toString()] as Rap
          if (rap !== undefined) {
            let orgcods = rap.orgcods
            if (orgcods === undefined) {
              orgcods = rap.orgcods = []
            }
            orgcods.push(raporg.orgcod)
          }
        }
      }
    }
    if (this.follow.has("texte.ecrs")) {
      const ecrByEcrnum = this.objectByIdByTableName.ecr
      const texteById = this.objectByIdByTableName.texte
      if (ecrByEcrnum !== undefined && texteById !== undefined) {
        for (const ecr of Object.values(ecrByEcrnum) as Ecr[]) {
          const texcod = ecr.texcod
          if (texcod === null) {
            continue
          }
          const texte = texteById[texcod] as Texte
          if (texte !== undefined) {
            let ecrnums = texte.ecrnums
            if (ecrnums === undefined) {
              ecrnums = texte.ecrnums = []
            }
            ecrnums.push(ecr.ecrnum)
          }
        }
      }
    }
    if (this.follow.has("texte.txtAmeli")) {
      const sesById = this.objectByIdByTableName.ses
      const texteById = this.objectByIdByTableName.texte
      const txtAmeliById = this.objectByIdByTableName.txt_ameli
      if (sesById !== undefined && texteById !== undefined && txtAmeliById !== undefined) {
        for (const txtAmeli of Object.values(txtAmeliById) as TxtAmeli[]) {
          const sesinsid = txtAmeli.sesinsid
          const num = parseInt(txtAmeli.num)
          if (sesinsid !== null) {
            const sesins = sesById[sesinsid] as Ses
            if (sesins !== undefined) {
              for (const texte of Object.values(texteById) as Texte[]) {
                if (texte.sesann === sesins.ann && texte.texnum === num) {
                  texte.txtAmeliId = txtAmeli.id
                }
              }
            }
          }
        }
      }
    }
    if (this.follow.has("txtAmeli.subs")) {
      const subById = this.objectByIdByTableName.sub
      const txtAmeliById = this.objectByIdByTableName.txt_ameli
      if (subById !== undefined && txtAmeliById !== undefined) {
        for (const sub of Object.values(subById) as Sub[]) {
          const txtAmeli = txtAmeliById[sub.txtid] as TxtAmeli
          if (txtAmeli !== undefined) {
            let subids = txtAmeli.subids
            if (subids === undefined) {
              subids = txtAmeli.subids = []
            }
            subids.push(sub.id)
          }
        }
      }
    }
  }

  request(tableName: string, id: string): void {
    const visitedIds = this.visitedIdsByTableName[tableName]
    if (visitedIds === undefined || !visitedIds.has(id)) {
      let requestedIds = this.requestedIdsByTableName[tableName]
      if (requestedIds === undefined) {
        requestedIds = this.requestedIdsByTableName[tableName] = new Set()
      }
      requestedIds.add(id)
    }
  }

  toJson(): any {
    return this.objectByIdByTableName
  }
}
