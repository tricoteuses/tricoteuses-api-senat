import { Debat } from "@tricoteuses/senat"

import { dbByName, trimFieldsRight } from "../databases"

export const debatsFieldsToTrim = ["deburl", "libspec"]
export const lecassdebFieldsToTrim = ["lecassidt"]

export const getDebats = async (ids: string[]): Promise<Debat[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.debats.any(
    `
      SELECT *
      FROM debats
      WHERE datsea IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((debat: Debat) => trimFieldsRight(debatsFieldsToTrim, debat))
}

export const getDebatsFromLecassidts = async (ids: string[]): Promise<Debat[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.debats.any(
    `
      SELECT *
      FROM debats
      WHERE datsea IN (
        SELECT datsea
        FROM lecassdeb
        WHERE lecassidt IN ($<ids:list>)
      )
    `,
    {
      ids,
    },
  )).map((debat: Debat) => trimFieldsRight(debatsFieldsToTrim, debat))
}
