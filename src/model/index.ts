export {
  getLoi,
  getLois,
  getTexte,
  getTyplois,
  loiFieldsToTrim,
  retrieveTexteParsed,
  texteFieldsToParseInt,
  texteFieldsToTrim,
} from "./dosleg"
export { allFollows, DataGetter } from "./data_getters"
export { senFieldsToParseInt, senFieldsToTrim } from "./sens"
