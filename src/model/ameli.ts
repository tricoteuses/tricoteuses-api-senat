import { Ses, Sub, TxtAmeli } from "@tricoteuses/senat"

import { dbByName, parseIntFields, trimFieldsRight } from "../databases"

export const sesFieldsToParseInt = ["ann"]
export const sesFieldsToTrim = ["lil"]
export const subFieldsToParseInt = ["pos", "posder", "prires"]
export const subFieldsToTrim = ["lic", "lib", "sig", "style"]
export const txtAmeliFieldsToTrim = [
  "num",
  "int",
  "inl",
  "libdelim",
  "libcplnat",
  "doslegsignet",
  "ordsnddelib",
]

export const getSessFromAnns = async (ids: string[]): Promise<Ses[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.ameli.any(
    `
      SELECT *
      FROM ses
      WHERE ann IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((ses: Ses) =>
    parseIntFields(sesFieldsToParseInt, trimFieldsRight(sesFieldsToTrim, ses)),
  )
}

export const getSubsFromTxtids = async (ids: string[]): Promise<Sub[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.ameli.any(
    `
      SELECT *
      FROM sub
      WHERE txtid IN($<ids:list>)
    `,
    {
      ids,
    },
  )).map((sub: Sub) =>
    parseIntFields(subFieldsToParseInt, trimFieldsRight(subFieldsToTrim, sub)),
  )
}

export const getTxtsAmeliBySesannNumJoins = async (
  ids: string[],
): Promise<TxtAmeli[]> => {
  if (ids.length === 0) {
    return []
  }
  const txtsAmelis = []
  for (const id of ids) {
    const idSplit = id.split(" ")
    const sesann = parseInt(idSplit[0])
    const num = idSplit[1]
    const txtAmeli = trimFieldsRight(
      txtAmeliFieldsToTrim,
      await dbByName.ameli.oneOrNone(
        `
          SELECT *
          FROM txt_ameli
          WHERE
            sesinsid in (
              SELECT id
              FROM ses
              WHERE ann = $<sesann>
            ) AND
            num = $<num>
          LIMIT 1
        `,
        {
          num,
          sesann,
        },
      ),
    ) as TxtAmeli
    if (txtAmeli !== null) {
      txtsAmelis.push(txtAmeli)
    }
  }
  return txtsAmelis
}
