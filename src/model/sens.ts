import { Sen } from "@tricoteuses/senat"

import { dbByName, parseIntFields, trimFieldsRight } from "../databases"

export const senFieldsToParseInt = ["sencirnumcou", "sennumsie"]
export const senFieldsToTrim = [
  "senmat",
  "quacod",
  "sennomuse",
  "sennomtec",
  "senprenomuse",
  "etasencod",
  "sendespro",
  "pcscod",
  "catprocod",
  "sengrppolcodcou",
  "sengrppolliccou",
  "sentypappcou",
  "sencomcodcou",
  "sencomliccou",
  "sencircou",
  "senburliccou",
  "senema",
  "sennomusecap",
  "senfem",
  "sendaiurl",
]

export const getSens = async (ids: string[]): Promise<Sen[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.sens.any(
    `
      SELECT *
      FROM sen
      WHERE senmat IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((sen: Sen) =>
    parseIntFields(senFieldsToParseInt, trimFieldsRight(senFieldsToTrim, sen)),
  )
}
