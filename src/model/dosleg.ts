import {
  Ass,
  Aud,
  Auteur,
  DateSeance,
  DecCoc,
  DenRap,
  DocAtt,
  Ecr,
  EtaLoi,
  LecAss,
  LecAssRap,
  Lecture,
  Loi,
  Org,
  OriTxt,
  Qua,
  Rap,
  Scr,
  Texte,
  TypAtt,
  TypLec,
  TypLoi,
  TypTxt,
  TypUrl,
} from "@tricoteuses/senat"
import assert from "assert"
import fetch from "cross-fetch"
import { JSDOM } from "jsdom"

import { dbByName, parseIntFields, trimFieldsRight } from "../databases"

export const assFieldsToTrim = ["libass"]
export const audFieldsToTrim = ["lecassidt", "audtit", "audurl", "orgcod"]
export const auteurFieldsToTrim = [
  "autcod",
  "quacod",
  "typautcod",
  "nomuse",
  "prenom",
  "nomtec",
  "autmat",
  "autfct",
]
export const dateSeanceFieldsToTrim = ["lecidt", "statut"]
export const deccocFieldsToTrim = ["deccoccod", "deccoclib"]
export const denrapFieldsToTrim = [
  "coddenrap",
  "typraprap",
  "libdenrap",
  "denrapmin",
  "denraptit",
  "denrapstymin",
  "solnatrapcod",
]
export const docattFieldsToParseInt = ["docattcle", "rapcod"]
export const docattFieldsToTrim = ["docatturl"]
export const ecrFieldsToTrim = ["autcod", "ecrqua"]
export const etaloiFieldsToTrim = ["etaloilib"]
export const lecassFieldsToTrim = [
  "lecassidt",
  "lecidt",
  "ptlurl",
  "ptlnumcpl",
  "ptlnot",
  "ptlurl2",
  "ptlnot2",
  "ptlurl3",
  "ptlnot3",
  "ptlnumcpl2",
  "ptlnumcpl3",
  "lecassame",
  "orgcod",
  "loiintmod",
  "reucom",
  "debatsurl",
  "libppr",
  "ptlurlcom",
  "aliasppr",
  "lecassamecom",
]
export const lecassrapFieldsToTrim = ["lecassidt"]
export const lectureFieldsToTrim = ["lecidt", "loicod", "typleccod", "leccom"]
export const loiFieldsToTrim = [
  "loicod",
  "typloicod",
  "deccoccod",
  "numero",
  "loient",
  "motclef",
  "loitit",
  "loiint",
  "url_jo",
  "loinumjo",
  "loititjo",
  "url_jo2",
  "loinumjo2",
  "deccocurl",
  "num_decision",
  "loicodmai",
  "loinoudelibcod",
  "motionloiorigcod",
  "url_ordonnance",
  "saisine_par",
  "loinumjo3",
  "url_jo3",
  "url_an",
  "url_presart",
  "signetalt",
  "orgcod",
  "doscocurl",
  "loiintori",
]
export const orgFieldsToTrim = [
  "orgcod",
  "typorgcod",
  "orgnom",
  "orgliblon",
  "orglibaff",
  "orgurl",
  "orglibcou",
  "org_de",
  "urltra",
  "inttra",
  "orgnomcouv",
  "senorgcod",
  "html_color",
]
export const oritxtFieldsToTrim = ["oritxtcod", "oritxtlib", "oritxtlibfem"]
export const quaFieldsToTrim = ["quacod", "qualic", "quaabr", "quaabrplu"]
export const rapFieldsToParseInt = ["rapcod", "sesann", "rapnum", "rapnuman"]
export const rapFieldsToTrim = [
  "coddenrap",
  "blecod",
  "raptitcou",
  "raptil",
  "rapurl",
  "url2",
  "url3",
  "url4",
  "url2txt",
  "url3txt",
  "url4txt",
  "prix",
  "numerobis",
  "rapsoustit",
  "rapres",
  "forpubcod",
]
export const raporgFieldsToTrim = ["orgcod"]
export const scrFieldsToTrim = ["scrint", "soslib"]
export const texteFieldsToParseInt = ["texcod", "sesann", "texnum"]
export const texteFieldsToTrim = [
  "oritxtcod",
  "typtxtcod",
  "lecassidt",
  "orgcod",
  "texurl",
  "url2",
  "url3",
  "url4",
  "url2txt",
  "url3txt",
  "url4txt",
  "prix",
  "numerobis",
  "reserve_comspe",
]
export const typattFieldsToTrim = ["typattlib"]
export const typlecFieldsToTrim = ["typleccod", "typleclib"]
export const typloiFieldsToTrim = [
  "typloicod",
  "typloilib",
  "groupe",
  "typloiden",
  "typloigen",
  "typloitit",
  "typloidenplu",
  "typloide",
  "typloiabr",
]
export const typtxtFieldsToTrim = ["typtxtcod", "typtxtlib"]
export const typurlFieldsToTrim = ["libtypurl"]

export const getAsss = async (ids: string[]): Promise<Ass[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM ass
      WHERE codass IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((ass: Ass) => trimFieldsRight(assFieldsToTrim, ass))
}

export const getAudsFromLecassidts = async (ids: string[]): Promise<Aud[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM aud
      WHERE lecassidt IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((aud: Aud) => trimFieldsRight(audFieldsToTrim, aud))
}

export const getAuteurs = async (ids: string[]): Promise<Auteur[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM auteur
      WHERE autcod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((auteur: Auteur) => trimFieldsRight(auteurFieldsToTrim, auteur))
}

export const getDatesSeancesFromLecassidts = async (
  ids: string[],
): Promise<DateSeance[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM date_seance
      WHERE lecidt IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((dateSeance: DateSeance) => trimFieldsRight(dateSeanceFieldsToTrim, dateSeance))
}

export const getDeccocs = async (ids: string[]): Promise<DecCoc[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM deccoc
      WHERE deccoccod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((deccoc: DecCoc) => trimFieldsRight(deccocFieldsToTrim, deccoc))
}

export const getDenraps = async (ids: string[]): Promise<DenRap[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM denrap
      WHERE coddenrap IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((denrap: DenRap) => trimFieldsRight(denrapFieldsToTrim, denrap))
}

export const getDocattsFromRapcods = async (ids: string[]): Promise<DocAtt[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM docatt
      WHERE rapcod IN($<ids:list>)
    `,
    {
      ids,
    },
  )).map((docatt: DocAtt) =>
    parseIntFields(docattFieldsToParseInt, trimFieldsRight(docattFieldsToTrim, docatt)),
  )
}

export const getEcrs = async (ids: string[]): Promise<Ecr[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM ecr
      WHERE ecrnum IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((ecr: Ecr) => trimFieldsRight(ecrFieldsToTrim, ecr))
}

export const getEcrsFromRapcods = async (ids: string[]): Promise<Ecr[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM ecr
      WHERE rapcod IN($<ids:list>)
    `,
    {
      ids,
    },
  )).map((ecr: Ecr) => trimFieldsRight(ecrFieldsToTrim, ecr))
}

export const getEcrsFromTexcods = async (ids: string[]): Promise<Ecr[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM ecr
      WHERE texcod IN($<ids:list>)
    `,
    {
      ids,
    },
  )).map((ecr: Ecr) => trimFieldsRight(ecrFieldsToTrim, ecr))
}

export const getEtalois = async (ids: string[]): Promise<EtaLoi[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM etaloi
      WHERE etaloicod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((etaloi: EtaLoi) => trimFieldsRight(etaloiFieldsToTrim, etaloi))
}

export const getLecasssFromLecidts = async (ids: string[]): Promise<LecAss[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM lecass
      WHERE lecidt IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((lecass: LecAss) => trimFieldsRight(lecassFieldsToTrim, lecass))
}

export const getLecassrapsFromLecassidts = async (
  ids: string[],
): Promise<LecAssRap[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM lecassrap
      WHERE lecassidt IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((lecassrap: LecAssRap) => trimFieldsRight(lecassrapFieldsToTrim, lecassrap))
}

export const getLecturesFromLoicods = async (ids: string[]): Promise<Lecture[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM lecture
      WHERE loicod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((lecture: Lecture) => trimFieldsRight(lectureFieldsToTrim, lecture))
}

export const getLoi = async (id: string): Promise<Loi> => {
  return trimFieldsRight(
    loiFieldsToTrim,
    await dbByName.dosleg.oneOrNone(
      `
        SELECT *
        FROM loi
        where loicod = $<id>
      `,
      {
        id,
      },
    ),
  ) as Loi
}

export const getLois = async (ids: string[]): Promise<Loi[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM loi
      WHERE loicod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((loi: Loi) => trimFieldsRight(loiFieldsToTrim, loi))
}

export const getOrgs = async (ids: string[]): Promise<Org[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM org
      WHERE orgcod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((org: Org) => trimFieldsRight(orgFieldsToTrim, org))
}

export const getOrgsFromRapcods = async (ids: string[]): Promise<Org[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM org
      WHERE orgcod IN (
        SELECT orgcod
        FROM raporg
        WHERE rapcod IN ($<ids:list>)
      )
    `,
    {
      ids,
    },
  )).map((org: Org) => trimFieldsRight(orgFieldsToTrim, org))
}

export const getOritxts = async (ids: string[]): Promise<OriTxt[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM oritxt
      WHERE oritxtcod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((oritxt: OriTxt) => trimFieldsRight(oritxtFieldsToTrim, oritxt))
}

export const getQuas = async (ids: string[]): Promise<Qua[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM qua
      WHERE quacod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((qua: Qua) => trimFieldsRight(quaFieldsToTrim, qua))
}

export const getRaps = async (ids: string[]): Promise<Rap[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM rap
      WHERE rapcod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((rap: Rap) =>
    parseIntFields(rapFieldsToParseInt, trimFieldsRight(rapFieldsToTrim, rap)),
  )
}

export const getScrsFromCodes = async (ids: string[]): Promise<Scr[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM scr
      WHERE code IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((scr: Scr) => trimFieldsRight(scrFieldsToTrim, scr))
}

export const getTexte = async (id: string): Promise<Texte> => {
  return parseIntFields(
    texteFieldsToParseInt,
    trimFieldsRight(
      texteFieldsToTrim,
      await dbByName.dosleg.oneOrNone(
        `
          SELECT *
          FROM texte
          where texcod = $<id>
        `,
        {
          id,
        },
      ),
    ),
  ) as Texte
}

export const getTextesFromLecassidts = async (ids: string[]): Promise<Texte[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM texte
      WHERE lecassidt IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((texte: Texte) =>
    parseIntFields(texteFieldsToParseInt, trimFieldsRight(texteFieldsToTrim, texte)),
  )
}

export const getTypatts = async (ids: string[]): Promise<TypAtt[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM typatt
      WHERE typattcod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((typatt: TypAtt) => trimFieldsRight(typattFieldsToTrim, typatt))
}

export const getTyplecs = async (ids: string[]): Promise<TypLec[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM typlec
      WHERE typleccod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((typlec: TypLec) => trimFieldsRight(typlecFieldsToTrim, typlec))
}

export const getTyplois = async (ids: string[]): Promise<TypLoi[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM typloi
      WHERE typloicod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((typloi: TypLoi) => trimFieldsRight(typloiFieldsToTrim, typloi))
}

export const getTyptxts = async (ids: string[]): Promise<TypTxt[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM typtxt
      WHERE typtxtcod IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((typtxt: TypTxt) => trimFieldsRight(typtxtFieldsToTrim, typtxt))
}

export const getTypurls = async (ids: string[]): Promise<TypUrl[]> => {
  if (ids.length === 0) {
    return []
  }
  return (await dbByName.dosleg.any(
    `
      SELECT *
      FROM typurl
      WHERE typurl IN ($<ids:list>)
    `,
    {
      ids,
    },
  )).map((typurl: TypUrl) => trimFieldsRight(typurlFieldsToTrim, typurl))
}

export function parseTexte(senatUrl: string, page: string, sigs: string[]) {
  console.log(page)
  const match = page.match(/<\?xml .*$/m)
  if (match === null) {
    console.error(`Page "${senatUrl}" doesn't seem to embed another HTML page`)
    return {
      error: {
        code: -1,
        message: `La page "${senatUrl}" semble ne pas contenir de sous-page HTML`,
      },
      page,
      url: senatUrl,
    }
  }
  const embedHtml = match[0]

  // Extract subdivisions from HTML.
  const { window } = new JSDOM(embedHtml)
  const { document } = window
  const alineasRootDivElement = document.getElementById(
    "formCorrection:tableauComparatif",
  )
  assert.notStrictEqual(alineasRootDivElement, null)
  assert.strictEqual(alineasRootDivElement!.tagName, "DIV")
  const subdivisionsIds = new Set(sigs)
  const subdivisionById: { [id: string]: any } = {}
  let subdivisionAlineasHtml: string[] | null = null
  let subdivisionAlineasText: string[] | null = null
  let subdivisionHeadersHtml: string[] | null = null
  let subdivisionHeadersText: string[] | null = null
  for (let tbodyElement of alineasRootDivElement!.getElementsByTagName("tbody")) {
    for (let trElement of tbodyElement.children) {
      // Try to retrieve a subdivision ID.
      const pastilleTdElement = trElement.children[0]
      const pastilleDivElements = [...pastilleTdElement.children]
      assert.strictEqual(pastilleDivElements.length, 1)
      const pastilleDivElement = pastilleDivElements[0]
      assert.strictEqual(pastilleDivElement.tagName, "DIV")
      const pastillePElements = [...pastilleDivElement.children]
      assert.strictEqual(pastillePElements.length, 1)
      const pastillePElement = pastillePElements[0]
      assert.strictEqual(pastillePElement.tagName, "P")
      const alineaId = pastillePElement.getAttribute("id")
      if (alineaId !== null && subdivisionsIds.has(alineaId)) {
        subdivisionAlineasHtml = []
        subdivisionAlineasText = []
        subdivisionHeadersHtml = []
        subdivisionHeadersText = []
        subdivisionById[alineaId] = {
          html: {
            alineas: subdivisionAlineasHtml,
            headers: subdivisionHeadersHtml,
          },
          text: {
            alineas: subdivisionAlineasText,
            headers: subdivisionHeadersText,
          },
        }
      }
      if (subdivisionAlineasHtml === null) {
        continue
      }
      const alineaTdElement = trElement.children[2]
      const alineaDivElements = [...alineaTdElement.children]
      assert.strictEqual(alineaDivElements.length, 1)
      const alineaDivElement = alineaDivElements[0]
      assert.strictEqual(alineaDivElement.tagName, "DIV")
      const alineaPElements = [...alineaDivElement.children]
      for (let alineaPElement of alineaPElements) {
        assert.strictEqual(alineaPElement.tagName, "P")
        const style = alineaPElement.getAttribute("style")
        assert.notStrictEqual(style, null)
        if (style!.includes("text-align: center")) {
          subdivisionHeadersHtml!.push(alineaPElement.outerHTML)
          subdivisionHeadersText!.push(alineaPElement.textContent!.trim())
        } else {
          subdivisionAlineasHtml!.push(alineaPElement.outerHTML)
          subdivisionAlineasText!.push(alineaPElement.textContent!.trim())
        }
      }
    }
  }
  window.close() // Free memory.

  return {
    error: null,
    html: embedHtml,
    page,
    subdivisions: subdivisionById,
    url: senatUrl,
  }
}

export async function retrieveTexteParsed(senatUrl: string, sigs: string[]) {
  const response = await fetch(senatUrl)
  const page = await response.text()
  if (!response.ok) {
    return {
      error: { code: response.status, message: response.statusText },
      page,
    }
  }
  return parseTexte(senatUrl, page, sigs)
}
