import { execSync } from "child_process"
import fs from "fs-extra"
import path from "path"

import { checkDatabase } from "./databases"

export interface Dataset {
  database: string
  patch?: (
    dataset: Dataset,
    dataDir: string,
    options: { sudo?: boolean; user?: string },
  ) => void
  repairEncoding: boolean
  repairZip?: (dataset: Dataset, dataDir: string) => void
  schema: string
  title: string
  url: string
}

export interface Datasets {
  ameli: Dataset
  debats: Dataset
  dosleg: Dataset
  questions: Dataset
  sens: Dataset
}

export enum EnabledDatasets {
  None = 0,
  Ameli = 1 << 0,
  Debats = 1 << 1,
  DosLeg = 1 << 2,
  Questions = 1 << 3,
  Sens = 1 << 4,
  PhotosSenateurs = 1 << 5,
  All = Ameli | Debats | DosLeg | Questions | Sens | PhotosSenateurs,
}

export const datasets: Datasets = {
  ameli: {
    database: "ameli",
    repairEncoding: false,
    repairZip: (dataset: Dataset, dataDir: string) => {
      const sqlFilename = `${dataset.database}.sql`
      const sqlFilePath = path.join(dataDir, sqlFilename)
      fs.removeSync(sqlFilePath)
      fs.moveSync(path.join(dataDir, "var", "opt", "opendata", sqlFilename), sqlFilePath)
    },
    schema: "public",
    title: "Amendements",
    url: "https://data.senat.fr/data/ameli/ameli.zip",
  },
  debats: {
    database: "debats",
    repairEncoding: true,
    schema: "public",
    title: "Informations relatives aux comptes rendus intégraux de la séance publique",
    url: "https://data.senat.fr/data/debats/debats.zip",
  },
  dosleg: {
    database: "dosleg",
    patch: async (
      dataset: Dataset,
      dataDir: string,
      options: { sudo?: boolean; user?: string },
    ) => {
      let command = `psql -c "CREATE EXTENSION IF NOT EXISTS pg_trgm" ${dataset.database}`
      if (options.sudo) {
        if (options.user) {
          command = `sudo -u ${options.user} ${command}`
        } else {
          command = `sudo ${command}`
        }
      }
      execSync(command, {
        cwd: dataDir,
        env: process.env,
        encoding: "utf-8",
        // stdio: ["ignore", "ignore", "pipe"],
      })

      const db = await checkDatabase(dataset.database)

      await db.none(
        `CREATE TABLE IF NOT EXISTS tricot_loi (
          loicod character(12) NOT NULL PRIMARY KEY REFERENCES loi(loicod) ON DELETE CASCADE,
          date_dernier_acte timestamptz,
          date_prochain_acte timestamptz
        )`,
      )
      await db.none(
        `COMMENT ON TABLE tricot_loi IS
          'informations calculées par Tricoteuses en complément de la table "loi"'
        `,
      )

      await db.none(
        `
          CREATE TABLE IF NOT EXISTS tricot_loi_autocomplete (
            loicod character(12) NOT NULL REFERENCES loi(loicod) ON DELETE CASCADE,
            autocomplete text NOT NULL,
            PRIMARY KEY (loicod, autocomplete)
          )
        `,
      )
      await db.none(
        `
          CREATE INDEX IF NOT EXISTS tricot_loi_autocomplete_trigrams_idx
          ON tricot_loi_autocomplete
          USING GIST (autocomplete gist_trgm_ops)
        `,
      )
      await db.none(
        `
          CREATE TABLE IF NOT EXISTS tricot_loi_text_search (
            loicod character(12) NOT NULL REFERENCES loi(loicod) ON DELETE CASCADE,
            text_search tsvector NOT NULL,
            PRIMARY KEY (loicod, text_search)
          )
        `,
      )
      await db.none(
        `
          CREATE INDEX IF NOT EXISTS tricot_loi_text_search_idx
          ON tricot_loi_text_search
          USING GIN (text_search)
        `,
      )

      const now = new Date()
      const lois = await db.any("SELECT * FROM loi")
      for (const loi of lois) {
        const titles: Set<string> = new Set()
        if (loi.loient !== null) {
          titles.add(loi.loient)
          if (loi.motclef !== null) {
            titles.add(`${loi.loient} ${loi.motclef}`)
          }
        }
        if (loi.loitit !== null) {
          titles.add(loi.loitit)
        }
        if (loi.loiint !== null) {
          titles.add(loi.loiint)
        }
        if (loi.loititjo !== null) {
          titles.add(loi.loititjo)
        }
        if (loi.loiintori !== null) {
          titles.add(loi.loiintori)
        }

        let dateDernierActe: Date | null = null
        let dateProchainActe: Date | null = null

        const lectures = await db.any("SELECT * FROM lecture WHERE loicod = $<loicod>", {
          loicod: loi.loicod,
        })
        for (const lecture of lectures) {
          const lecasss = await db.any("SELECT * FROM lecass WHERE lecidt = $<lecidt>", {
            lecidt: lecture.lecidt,
          })
          for (const lecass of lecasss) {
            const auds = await db.any(
              "SELECT * FROM aud WHERE lecassidt = $<lecassidt>",
              {
                lecassidt: lecass.lecassidt,
              },
            )
            for (const aud of auds) {
              if (aud.auddat < now) {
                if (dateDernierActe === null || dateDernierActe < aud.auddat) {
                  dateDernierActe = aud.auddat
                }
              } else {
                if (dateProchainActe === null || dateProchainActe > aud.auddat) {
                  dateProchainActe = aud.auddat
                }
              }
            }

            const datesSeances = await db.any(
              "SELECT * FROM date_seance WHERE lecidt = $<lecassidt>",
              {
                lecassidt: lecass.lecassidt,
              },
            )
            for (const dateSeance of datesSeances) {
              if (dateSeance.date_s === null) {
                continue
              }
              if (dateSeance.date_s < now) {
                if (dateDernierActe === null || dateDernierActe < dateSeance.date_s) {
                  dateDernierActe = dateSeance.date_s
                }
              } else {
                if (dateProchainActe === null || dateProchainActe > dateSeance.date_s) {
                  dateProchainActe = dateSeance.date_s
                }
              }
            }
          }
        }

        await db.none(
          `
            INSERT INTO tricot_loi (
              loicod,
              date_dernier_acte,
              date_prochain_acte
            )
            VALUES (
              $<loicod>,
              $<dateDernierActe>,
              $<dateProchainActe>
            )
          `,
          {
            dateDernierActe,
            dateProchainActe,
            loicod: loi.loicod,
          },
        )

        for (const title of titles) {
          await db.none(
            `
              INSERT INTO tricot_loi_autocomplete (
                loicod,
                autocomplete
              )
              VALUES (
                $<loicod>,
                $<autocomplete>
              )
              ON CONFLICT DO NOTHING
            `,
            {
              autocomplete: title,
              loicod: loi.loicod,
            },
          )
          await db.none(
            `
              INSERT INTO tricot_loi_text_search (
                loicod,
                text_search
              )
              VALUES (
                $<loicod>,
                setweight(to_tsvector('french', $<textSearch>), 'A')
              )
              ON CONFLICT DO NOTHING
            `,
            {
              textSearch: title,
              loicod: loi.loicod,
            },
          )
        }
      }
    },
    repairEncoding: true,
    schema: "public",
    title: "Dossiers législatifs",
    url: "https://data.senat.fr/data/dosleg/dosleg.zip",
  },
  questions: {
    database: "questions",
    repairEncoding: true,
    schema: "questions",
    title: "Questions écrites et orales posées par les sénateurs au Gouvernement",
    url: "https://data.senat.fr/data/questions/questions.zip",
  },
  sens: {
    database: "sens",
    repairEncoding: true,
    patch: async (
      dataset: Dataset,
      _dataDir: string,
      _options: { sudo?: boolean; user?: string },
    ) => {
      const db = await checkDatabase(dataset.database)

      await db.none(
        `CREATE TABLE IF NOT EXISTS tricot_photos (
          senmat character(6) NOT NULL PRIMARY KEY REFERENCES sen(senmat) ON DELETE CASCADE,
          data jsonb
        )`,
      )
      for (const command of [
        `COMMENT ON TABLE tricot_photos IS
          'informations sur les photographies officielles des sénateurs'
        `,
        "COMMENT ON COLUMN tricot_photos.senmat IS 'numéro de matricule d''un sénateur'",
        "COMMENT ON COLUMN tricot_photos.data IS 'JSON data of photo'",
      ]) {
        await db.none(command)
      }
    },
    repairZip: (dataset: Dataset, dataDir: string) => {
      const sqlFilename = `${dataset.database}.sql`
      const sqlFilePath = path.join(dataDir, sqlFilename)
      fs.removeSync(sqlFilePath)
      fs.moveSync(path.join(dataDir, "export_sens.sql"), sqlFilePath)
    },
    schema: "public",
    title: "Sénateurs (y compris organes et présence)",
    url: "https://data.senat.fr/data/senateurs/export_sens.zip",
  },
}
