require("dotenv").config()

import { validateServerConfig } from "./validators/server_config"

const serverConfig = {
  dataDir: process.env.TRICOTEUSES_SENAT_API_DATA_DIR,
  db: {
    host: process.env.TRICOTEUSES_SENAT_API_DB_HOST || "localhost",
    password: process.env.TRICOTEUSES_SENAT_API_DB_PASSWORD || "opendata",
    port: process.env.TRICOTEUSES_SENAT_API_DB_PORT || 5432,
    user: process.env.TRICOTEUSES_SENAT_API_DB_USER || "opendata",
  },
  listen: {
    host: null, // Listen to every IPv4 addresses.
    port: null, // Listen to config.port by default
  },
  port: process.env.TRICOTEUSES_SENAT_API_PORT || 1792,
  proxy: process.env.TRICOTEUSES_SENAT_API_PROXY || false, // Is this application used behind a trusted proxy?
  sessionSecret: "Tricoteuses secret", // Change it!
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      validServerConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
