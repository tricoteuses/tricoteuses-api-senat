import { Loi } from "@tricoteuses/senat"
import querystring from "querystring"

import {
  validateDateIso8601String,
  validateInteger,
  validateMaybeTrimmedString,
  validateMissing,
  validateOption,
  validateSetValue,
  validateStringToNumber,
  validateTest,
} from "@biryani/core"
import { NextFunction, Request, Response } from "express"

import { dbByName, trimFieldsRight } from "../databases"
import { wrapAsyncMiddleware } from "../middlewares"
import { allFollows, DataGetter, loiFieldsToTrim, getLoi } from "../model"
import { validateFollow } from "../validators/query"

interface LoiRequest extends Request {
  loi: Loi
}

export const accessLoi = wrapAsyncMiddleware(async function accessLoi(
  req: LoiRequest,
  res: Response,
) {
  const [query, error] = validateAccessLoiQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { loi } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addLoi(loi)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), id: loi.loicod }, null, 2))
})

export const listLois = wrapAsyncMiddleware(async function listLois(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListLoisQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { date, follow, limit, offset, q: term, session: firstSessionYear } = query

  const joinClauses: string[] = []
  const whereClauses: string[] = []
  if (date !== null) {
    whereClauses.push("(date_dernier_acte >= $<date> OR date_prochain_acte NOTNULL)")
  }
  if (firstSessionYear !== null) {
    whereClauses.push(`loi.loicod IN (
      SELECT DISTINCT loicod
      FROM lecture
      WHERE lecidt IN (
        SELECT DISTINCT lecidt
        FROM lecass
        WHERE sesann >= $<firstSessionYear>
      )
    )`)
  }
  if (term !== null) {
    whereClauses.push(`loi.loicod IN (
      SELECT loicod
      FROM tricot_loi_text_search
      WHERE text_search @@ plainto_tsquery('french', $<term>)
    )`)
  }
  const joinsClause = joinClauses.length > 0 ? joinClauses.join(" ") : ""
  const whereClause = whereClauses.length > 0 ? "WHERE " + whereClauses.join(" AND ") : ""

  const lois = (await dbByName.dosleg.any(
    `
      SELECT loi.*
      FROM loi
      INNER JOIN tricot_loi ON loi.loicod = tricot_loi.loicod
      ${joinsClause}
      ${whereClause}
      ORDER BY
        tricot_loi.date_prochain_acte DESC NULLS LAST,
        tricot_loi.date_dernier_acte DESC NULLS LAST
      OFFSET $<offset>
      LIMIT $<limit>
    `,
    {
      date,
      firstSessionYear,
      limit: limit + 1,
      offset,
      term,
    },
  )).map((loi: Loi) => trimFieldsRight(loiFieldsToTrim, loi))

  let next = null
  if (lois.length > limit) {
    lois.pop()

    const nextQuery = Object.entries({
      ...query,
      follow: Array.from(follow).sort(),
      offset: offset + limit,
    }).reduce((nextQuery: { [key: string]: any }, [key, value]) => {
      if (value !== null && (!Array.isArray(value) || value.length > 0)) {
        nextQuery[key] = value
      }
      return nextQuery
    }, {})
    next = `${req.protocol}://${req.get("host")}${
      req.originalUrl.split("?")[0]
    }?${querystring.stringify(nextQuery)}`
  }

  const dataGetter = new DataGetter(follow)
  for (const loi of lois) {
    dataGetter.addLoi(loi)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        ids: lois.map(({ loicod }: { loicod: string }) => loicod),
        next,
      },
      null,
      2,
    ),
  )
})

export const requireLoi = wrapAsyncMiddleware(async function requireLoi(
  req: LoiRequest,
  res: Response,
  next: NextFunction,
) {
  const { loicod } = req.params

  const loi = await getLoi(loicod)
  if (loi === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No loi with id "${loicod}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.loi = loi

  return next()
})

function validateAccessLoiQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListLoisQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "date"
    remainingKeys.delete(key)
    const [value, error] = validateOption(validateMissing, validateDateIso8601String)(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "limit"
    remainingKeys.delete(key)
    const [value, error] = validateOption(
      [validateMissing, validateSetValue(10)],
      [
        validateStringToNumber,
        validateInteger,
        validateTest(value => value >= 1, "Value must be greater than or equal to 1"),
        validateTest(value => value <= 100, "Value must be less than or equal to 100"),
      ],
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "offset"
    remainingKeys.delete(key)
    const [value, error] = validateOption(
      [validateMissing, validateSetValue(0)],
      [
        validateStringToNumber,
        validateInteger,
        validateTest(value => value >= 0, "Value must be greater than or equal to 0"),
      ],
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "q"
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "session"
    remainingKeys.delete(key)
    const [value, error] = validateOption(validateMissing, [
      validateStringToNumber,
      validateInteger,
      validateTest(value => value >= 1789, "Value must be greater than or equal to 1789"),
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
