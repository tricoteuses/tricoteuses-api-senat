import { Request, Response } from "express"

import { dbByName } from "../databases"
import { wrapAsyncMiddleware } from "../middlewares"

export const listSenateurs = wrapAsyncMiddleware(async function listSenateurs(
  _req: Request,
  res: Response,
) {
  const senateurs = await dbByName.sens.any(
    `
      SELECT *
      FROM sen
      LIMIT 10
    `,
  )

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        senateurs,
      },
      null,
      2,
    ),
  )
})
