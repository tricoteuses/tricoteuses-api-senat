import { NextFunction, Request, Response } from "express"

import { dbByName, parseIntFields, trimFieldsRight } from "../databases"
import { wrapAsyncMiddleware } from "../middlewares"
import {
  allFollows,
  DataGetter,
  getTexte,
  retrieveTexteParsed,
  Sub,
  Texte,
  texteFieldsToParseInt,
  texteFieldsToTrim,
} from "../model"
import { validateFollow } from "../validators/query"

interface TexteRequest extends Request {
  texte: Texte
}

export const accessContenuTexte = wrapAsyncMiddleware(async function accessContenuTexte(
  req: TexteRequest,
  res: Response,
) {
  const [query, error] = validateAccessContenuTexteQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }

  const { texte } = req

  const dataGetter = new DataGetter(new Set(["texte.txtAmeli", "txtAmeli.subs"]))
  dataGetter.addTexte(texte)
  await dataGetter.getAll()
  const subById: { [id: string]: Sub } = dataGetter.toJson().sub
  const sigs = Object.values(subById)
    .map(sub => sub.sig)
    .filter(sig => sig !== null) as string[]

  if (texte.texurl === null) {
    res.writeHead(200, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: { code: 404, message: "Le texte n'a pas d'URL au Sénat" },
        },
        null,
        2,
      ),
    )
  }

  // Note: Don't use HTTPS Sénat URL yet, because the Sénat HTTPS server is
  // obsolete and OpenSSL negociation fails with error:
  // SSL routines:tls_process_ske_dhe:dh key too small.
  const senatUrl = new URL(texte.texurl, "http://www.senat.fr/leg/").toString()
  const texteLoiParsed = await retrieveTexteParsed(senatUrl, sigs)

  const { error: parseError, page } = texteLoiParsed
  if (parseError) {
    console.error(
      `Error while getting page "${senatUrl}":\n\nError:\n${JSON.stringify(
        parseError,
        null,
        2,
      )}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            details: parseError,
            message: `Retrieval of HTML page ${senatUrl} failed`,
            page,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(texteLoiParsed, null, 2))
})

export const accessTexte = wrapAsyncMiddleware(async function accessTexte(
  req: TexteRequest,
  res: Response,
) {
  const [query, error] = validateAccessTexteQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { texte } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addTexte(texte)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), id: texte.texcod }, null, 2))
})

export const listTextes = wrapAsyncMiddleware(async function listTextes(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListTextesQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const textes: Texte[] = (await dbByName.dosleg.any(
    `
      SELECT *
      FROM texte
      LIMIT 10
    `,
  )).map((texte: Texte) =>
    parseIntFields(texteFieldsToParseInt, trimFieldsRight(texteFieldsToTrim, texte)),
  )

  const dataGetter = new DataGetter(follow)
  for (const texte of textes) {
    dataGetter.addTexte(texte)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        ids: textes.map(({ texcod }) => texcod),
      },
      null,
      2,
    ),
  )
})

// export const listTexteLoiAmendements = wrapAsyncMiddleware(
//   async function listTexteLoiAmendements(req: Request, res: Response) {
//     const { texteUid } = req.params

//     const [query, error] = validateListTexteLoiAmendementsQuery(req.query)
//     if (error !== null) {
//       console.error(
//         `Error in ${req.path}:\n${JSON.stringify(
//           query,
//           null,
//           2,
//         )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
//       )
//       res.writeHead(400, {
//         "Content-Type": "application/json; charset=utf-8",
//       })
//       return res.end(
//         JSON.stringify(
//           {
//             ...query,
//             error: {
//               code: 400,
//               details: error,
//               message: "Invalid query",
//             },
//           },
//           null,
//           2,
//         ),
//       )
//     }
//     const { follow } = query

//     const amendements = (await db.any(
//       `
//         SELECT *
//         FROM amendements
//         WHERE
//           texte_loi_uid = $<texteUid>
//         ORDER BY tri
//       `,
//       {
//         texteUid,
//       },
//     )).map(extractDataFromEntry)

//     const dataGetter = new DataGetter(follow)
//     for (const amendement of amendements) {
//       dataGetter.addAmendement(amendement)
//     }
//     await dataGetter.getAll()

//     res.writeHead(200, {
//       "Content-Type": "application/json; charset=utf-8",
//     })
//     res.end(
//       JSON.stringify(
//         {
//           ...dataGetter.toJson(),
//           uids: amendements.map(({ uid }) => uid),
//         },
//         null,
//         2,
//       ),
//     )
//   },
// )

export const requireTexte = wrapAsyncMiddleware(async function requireTexte(
  req: TexteRequest,
  res: Response,
  next: NextFunction,
) {
  const { texcod } = req.params

  const texte = await getTexte(texcod)
  if (texte === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No texte with texcod "${texcod}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.texte = texte

  return next()
})

function validateAccessContenuTexteQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  // {
  //   const key = "follow"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateFollow(allFollows)(data[key])
  //   data[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateAccessTexteQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListTextesQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

// function validateListTexteLoiAmendementsQuery(data: any) {
//   if (data === null || data === undefined) {
//     return [data, "Missing value"]
//   }
//   if (typeof data !== "object") {
//     return [data, `Expected an object, got ${typeof data}`]
//   }

//   data = {
//     ...data,
//   }
//   const remainingKeys = new Set(Object.keys(data))
//   const errors: { [key: string]: any } = {}

//   {
//     const key = "follow"
//     remainingKeys.delete(key)
//     const [value, error] = validateFollow(allFollows)(data[key])
//     data[key] = value
//     if (error !== null) {
//       errors[key] = error
//     }
//   }

//   for (const key of remainingKeys) {
//     errors[key] = "Unexpected entry"
//   }
//   return [data, Object.keys(errors).length === 0 ? null : errors]
// }
