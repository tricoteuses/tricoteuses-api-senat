# Tricoteuses-API-Senat

This project has **migrated** to https://git.en-root.org/tricoteuses/tricoteuses-api-assemblee.

## _REST API based on open data of French Sénat_

## Installation

```bash
git clone https://framagit.org/tricoteuses/tricoteuses-api-senat
cd tricoteuses-api-senat/
```

Create a `.env` file to set PostgreSQL database informations and other configuration variables (you can use `example.env` as a template). Then

```bash
npm install
```

## Usage

### Retrieval, cleaning & import of open data from Sénat

```bash
npx babel-node --extensions ".ts" --max-old-space-size=4096 -- src/scripts/retrieve_open_data.ts --fetch --unzip --repair-encoding ../senat-data/
# Stop API server (because it is connected to database and blocks its reinitialization), then:
npx babel-node --extensions ".ts" -- src/scripts/retrieve_open_data.ts --import --patch --sudo --user postgres ../senat-data/
# Start API server.
```

## Retrieval of sénateurs' pictures from Sénat's website

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_senateurs_photos.ts --fetch ../senat-data/
```

### Retrieval & cleaning of bills from Sénat HTML pages

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_textes.ts ../senat-data/
```

### Launch development web server

```bash
npm run dev
```

### Generation of `@tricoteuses/senat` raw types from SQL schema.

Note: This updates the source code of `@tricoteuses/senat`.

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_open_data.ts --schema ../tricoteuses/senat/ ../senat-data/
```
